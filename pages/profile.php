<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 27/04/2019
 * Time: 16:24
 */

include_once "head.php";

session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

$url = $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$id = explode('=', $url)[1];
create_head($id);

$u = new User($id, $DATABASE);
?>
    <div class="coverPicture">
        <img src="images/<?php echo $u->getCoverPicture() == 0 ? '0_cp' : $u->getCoverPicture(); ?>.jpg" id="cover"/>
    </div>
    <div class="profileContainer">
        <div class="profilePicture">
            <img src="images/<?php echo $u->getProfilePicture(); ?>.jpg" id="profile"/>
            <h1><?php echo $u->getName(); ?></h1>
            <h2><?php echo $u->getBirthDay(); ?></h2>
            <?php if ($_SESSION["loggedInUser"] != $u->getId()) {
                echo '<button class="modifyData"><a href="../pages/messages.php?id='. $_SESSION['loggedInUser'] . '">Üzenet írása</a></button>';
                if (in_array($_SESSION['loggedInUser'], $u->getContacts())) { ?>
                    <form method="POST" action="fucntions/f_contact.php">
                        <input type="hidden" value="<?php echo $u->getId(); ?>">
                        <button type="submit" class="modifyData">Ismerősök vagytok <svg class="icon icon-user-check"><use xlink:href="#icon-user-check"></use>
                                <symbol id="icon-user-check" viewBox="0 0 32 32">
                                    <title>user-check</title>
                                    <path d="M30 19l-9 9-3-3-2 2 5 5 11-11z"></path>
                                    <path d="M14 24h10v-3.598c-2.101-1.225-4.885-2.066-8-2.321v-1.649c2.203-1.242 4-4.337 4-7.432 0-4.971 0-9-6-9s-6 4.029-6 9c0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h14v-2z"></path>
                                </symbol>
                            </svg></button>
                    </form>
                    <br>
                <?php } else { ?>
                    <form method="POST" action="functions/f_contact.php">
                        <input name="new_contact" type="hidden" value="<?php echo $u->getId(); ?>">
                        <button type="submit" class="modifyData">Ismerős hozzáadása <svg class="icon icon-user-check"><use xlink:href="#icon-user-check"></use>
                                <symbol id="icon-user-check" viewBox="0 0 32 32">
                                    <title>user-check</title>
                                    <path d="M30 19l-9 9-3-3-2 2 5 5 11-11z"></path>
                                    <path d="M14 24h10v-3.598c-2.101-1.225-4.885-2.066-8-2.321v-1.649c2.203-1.242 4-4.337 4-7.432 0-4.971 0-9-6-9s-6 4.029-6 9c0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h14v-2z"></path>
                                </symbol>
                            </svg></button>
                    </form>
                    <br>
                <?php }
            } ?>
        </div>
        <div class="profileTopic" onclick="showContent('images')">
            <h3>Képek</h3>
            <br><br>
            <svg class="icon icon-images"><use xlink:href="#icon-images"></use>
                <symbol id="icon-images" viewBox="0 0 36 32">
                    <title>images</title>
                    <path d="M34 4h-2v-2c0-1.1-0.9-2-2-2h-28c-1.1 0-2 0.9-2 2v24c0 1.1 0.9 2 2 2h2v2c0 1.1 0.9 2 2 2h28c1.1 0 2-0.9 2-2v-24c0-1.1-0.9-2-2-2zM4 6v20h-1.996c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v1.996h-24c-1.1 0-2 0.9-2 2v0zM34 29.996c-0.001 0.001-0.002 0.003-0.004 0.004h-27.993c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v23.993z"></path>
                    <path d="M30 11c0 1.657-1.343 3-3 3s-3-1.343-3-3 1.343-3 3-3 3 1.343 3 3z"></path>
                    <path d="M32 28h-24v-4l7-12 8 10h2l7-6z"></path>
                </symbol>
            </svg>
        </div>
        <div class="profileTopic" onclick="showContent('contacts')">
            <h3>Ismerősök</h3>
            <br><br>
            <svg class="icon icon-user"><use xlink:href="#icon-user"></use>
                <symbol id="icon-user" viewBox="0 0 32 32">
                    <title>user</title>
                    <path d="M18 22.082v-1.649c2.203-1.241 4-4.337 4-7.432 0-4.971 0-9-6-9s-6 4.029-6 9c0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h28c0-4.030-5.216-7.364-12-7.918z"></path>
                </symbol>
            </svg>
        </div>
        <div class="profileTopic" onclick="showContent('groups')">
            <h3>Csoportok</h3>
            <br><br>
            <svg class="icon icon-users"><use xlink:href="#icon-users"></use>
                <symbol id="icon-users" viewBox="0 0 36 32">
                    <title>users</title>
                    <path d="M24 24.082v-1.649c2.203-1.241 4-4.337 4-7.432 0-4.971 0-9-6-9s-6 4.029-6 9c0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h28c0-4.030-5.216-7.364-12-7.918z"></path>
                    <path d="M10.225 24.854c1.728-1.13 3.877-1.989 6.243-2.513-0.47-0.556-0.897-1.176-1.265-1.844-0.95-1.726-1.453-3.627-1.453-5.497 0-2.689 0-5.228 0.956-7.305 0.928-2.016 2.598-3.265 4.976-3.734-0.529-2.39-1.936-3.961-5.682-3.961-6 0-6 4.029-6 9 0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h8.719c0.454-0.403 0.956-0.787 1.506-1.146z"></path>
                </symbol>
            </svg>
        </div>
        <?php if ($id != $_SESSION["loggedInUser"]) { ?>
        <div class="profileTopic" onclick="window.location.replace('./messages.php?id=<?php echo $id ?>');">
            <h3>Üzenet küldése</h3>
            <br><br>
            <svg class="icon icon-bubbles3"><use xlink:href="#icon-bubbles3"></use>
                <symbol id="icon-bubbles3" viewBox="0 0 36 32">
                    <title>bubbles3</title>
                    <path d="M34 28.161c0 1.422 0.813 2.653 2 3.256v0.498c-0.332 0.045-0.671 0.070-1.016 0.070-2.125 0-4.042-0.892-5.398-2.321-0.819 0.218-1.688 0.336-2.587 0.336-4.971 0-9-3.582-9-8s4.029-8 9-8c4.971 0 9 3.582 9 8 0 1.73-0.618 3.331-1.667 4.64-0.213 0.463-0.333 0.979-0.333 1.522zM7.209 6.912c-2.069 1.681-3.209 3.843-3.209 6.088 0 1.259 0.35 2.481 1.039 3.63 0.711 1.185 1.781 2.268 3.093 3.133 0.949 0.625 1.587 1.623 1.755 2.747 0.056 0.375 0.091 0.753 0.105 1.129 0.233-0.194 0.461-0.401 0.684-0.624 0.755-0.755 1.774-1.172 2.828-1.172 0.168 0 0.336 0.011 0.505 0.032 0.655 0.083 1.323 0.125 1.987 0.126v4c-0.848-0-1.68-0.054-2.492-0.158-3.437 3.437-7.539 4.053-11.505 4.144v-0.841c2.142-1.049 4-2.961 4-5.145 0-0.305-0.024-0.604-0.068-0.897-3.619-2.383-5.932-6.024-5.932-10.103 0-7.18 7.163-13 16-13 8.702 0 15.781 5.644 15.995 12.672-1.284-0.572-2.683-0.919-4.133-1.018-0.36-1.752-1.419-3.401-3.070-4.742-1.104-0.897-2.404-1.606-3.863-2.108-1.553-0.534-3.211-0.804-4.928-0.804s-3.375 0.271-4.928 0.804c-1.46 0.502-2.76 1.211-3.863 2.108z"></path>
                </symbol>
            </svg>
        </div>
        <?php } else { ?>
        <div class="profileTopic" onclick="showContent('settings')">
            <h3>Adataid módosítása</h3>
            <br><br>
            <svg class="icon icon-pencil"><use xlink:href="#icon-pencil"></use>
                <symbol id="icon-pencil" viewBox="0 0 32 32">
                    <title>pencil</title>
                    <path d="M27 0c2.761 0 5 2.239 5 5 0 1.126-0.372 2.164-1 3l-2 2-7-7 2-2c0.836-0.628 1.874-1 3-1zM2 23l-2 9 9-2 18.5-18.5-7-7-18.5 18.5zM22.362 11.362l-14 14-1.724-1.724 14-14 1.724 1.724z"></path>
                </symbol>
            </svg>
        </div>
        <?php } ?>
        <div id="images" class="profileContent">
            <h1>Képek:</h1>
            <?php for ($i = 0; $i < sizeof($u->getImages()); $i++) { ?>
            <div class="picture">
                <img onclick="showImage('<?php echo $u->getImages()[$i]['IMAGE_ID'];?>')" src="images/<?php echo $u->getImages()[$i]['IMAGE_ID'];?>.jpg" width="500px" height="500px">
            </div>
            <?php }
            if ($_SESSION['loggedInUser'] == $id) { ?>
            <form action="functions/f_image_upload.php" method="post" enctype="multipart/form-data">
                <h2>Fénykép feltöltése</h2>
                <br>
                <input type="file" name="imageToUpload">
                <br>
                <br>
                <label>Fénykép leírása
                    <input type="text" name="description" required style="width: 30%">
                </label>
                <input type="submit" value="Feltöltés">
            </form>
            <?php } ?>
            <br><br>
        </div>
        <div id="contacts" class="profileContent">
            <h1>Ismerősök:</h1>
            <div class="w3-ul">
                <?php
                foreach ($u->getContacts() as $c) {
                    $u1 = new User($c, $DATABASE->getInstance());
                    $u1->to_string();
                }
                ?>
            </div>
        </div>
        <div id="groups" class="profileContent">
            <h1>Csoportok:</h1>
            <div class="w3-ul">
                <?php
                foreach ($u->getGroups() as $g) {
                    $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM CSOPORTOK WHERE ID = :id_bv");
                    oci_bind_by_name($stid, ":id_bv", $g);
                    oci_execute($stid);

                    $row = oci_fetch_assoc($stid);

                    echo "<li class='w3-indigo w3-card w3-padding w3-margin'>";
                    echo "<a href='group.php?id={$g}'>" . $row['NEV'] . "</a>";
                    echo "</li>";
                }
                ?>
            </div>
        </div>
        <div id="settings" class="profileContent">
            <h1>Adatmódosítás:</h1>
            <form action="functions/f_modify_data.php" method="post">
                <div style="margin: 2%">
                    <label for="updatedUserName"> Felhasználónév megváltoztatása
                        <input type="text" name="updatedUsername" id="edit_uname" required placeholder="<?php echo $u->getUserName(); ?>">
                    </label>
                </div>
                    <div style="margin: 2%">
                    <label for="updatedUserName"> Jelszó megváltoztatása
                        <input type="password" name="updatedPassword" required id="edit_pass" placeholder="<?php
                            for ($i = 0; $i < strlen($u->getPassword()); $i++) {
                                echo '*';
                            }
                        ?>">
                    </label>
                </div>
                <div style="margin: 2%">
                    <label for="updatedUserName"> Név megváltoztatása:
                        <input type="text" name="updatedName" required id="edit_name" placeholder="<?php echo $u->getName(); ?>">
                    </label>
                </div>
                <div style="margin: 2%">
                    <label for="updatedUserName"> Születésnap megváltoztatása
                        <input type="date" name="updatedBirthday" required id="edit_birthday" placeholder="<?php echo $u->getBirthDay(); ?>">
                    </label>
                </div>
                <input type="submit" value="Módosítás">
            </form>
        </div>
    </div>

    <div id="darkener"></div>
    <div id="imageViewer">
        <p class="closeButton" onclick="showImage(null)">+</p>
        <div class="image" id="imageHolder">

        </div>
        <div class="comments" id="commentHolder">
        </div>
    </div>

<script>
    function resetContent() {
        document.getElementById("images").style.display = 'none';
        document.getElementById("contacts").style.display = 'none';
        document.getElementById("groups").style.display = 'none';
        document.getElementById("settings").style.display = 'none';
    }

    function showContent(contentName) {
        if (document.getElementById(contentName).style.display !== 'block') {
            resetContent();
            document.getElementById(contentName).style.display = 'block';
            document.getElementById(contentName).style.marginBottom = 'none';
        } else {
            document.getElementById(contentName).style.display = 'none';
        }
    }

    function showImage(image) {
        if (document.getElementById('imageViewer').style.display === 'block') {
            document.getElementById('imageViewer').style.display = 'none';
            document.getElementById('darkener').style.display = 'none';
        } else {
            document.getElementById('imageViewer').style.display = 'block';
            document.getElementById('darkener').style.display = 'block';
        }
        if (image !== null) {
            var images = <?php echo json_encode($u->getImages()); ?>,
                description = "NULL";
            for (var i = 0; i < images.length; i++) {
                if (images[i].IMAGE_ID === image) {
                    description = images[i].IMAGE_DESC;
                }
            }
            document.getElementById('imageHolder').innerHTML = '<img src="images/' + image + '.jpg">';
            document.getElementById('commentHolder').innerHTML =
                '<div>' +
                '<h2>' + '<?php echo $u->getName();?>' + '</h2>' +
                '</div>' +
                '<div>' +
                '<p>' + description + '</p>' +
                '</div>' +
                <?php if ($_SESSION['loggedInUser'] == $id) { ?>
                '<div style="display: inline-block">' +
                '<form style="display: inline-block" method="post" action="functions/set_profile_picture.php">' +
                '<input type="hidden" name="picture" value="' + image +'">' +
                '<button class="modifyData" type="submit">Beállítás profilképként</button>' +
                '</form>' +
                '</div>' +
                '<div style="display: inline-block">' +
                '<form style="display: inline-block" method="post" action="functions/set_cover_picture.php">' +
                '<input type="hidden" name="picture" value="' + image +'">'+
                '<button class="modifyData" type="submit">Beállítás borítóképként</button>' +
                '</form>' +
                '</div>' +
                <?php } ?>
                '<div id="commentContainer">\n' +
                '</div>' +
                '<div id="addComment">' +
                '<form>' +
                '</form>' +
                '</div>';
            var comments = <?php
                $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM v_news_feed_comments");
                oci_execute($stid);
                echo json_encode(oci_fetch_array($stid));
                ?>;
            if (comments) {
                document.getElementById('commentContainer').innerHTML =
                    '<div class="comment">' +
                    'Jonapot' +
                    '</div>';
            }
        }
    }
</script>
