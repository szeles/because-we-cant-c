<?php

include_once 'head.php';
session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

create_head('groups');

$url = $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$id = explode('=', $url)[1];

$stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM CSOPORTOK WHERE ID = :id_bv");
oci_bind_by_name($stid, ':id_bv', $id);
oci_execute($stid);

$row = oci_fetch_assoc($stid);

$u = new User($_SESSION['loggedInUser'], $DATABASE->getInstance());
?>

<div class="w3-container w3-padding w3-margin">
    <div class="w3-half w3-white w3-round-large w3-padding">
        <h1>
            <?php echo $row['NEV']?>
        </h1>
        <hr>
        <div>
            <?php echo $row['LEIRAS']?>
        </div>
        <div>
            <a href="profile.php?id=<?php echo $row['ADMIN_ID'];?>"><?php echo $u->getName()?><a/>
        </div>
        <?php
        if (!in_array($id, $u->getGroups())) {
            echo "<hr>";
            echo '<form method="post" action="functions/f_join_group.php">';
            echo '<input type="hidden" name="csopnev" value="' . $id . '"/>';
            echo '<button class="w3-padding w3-indigo w3-hover-yellow" type="submit">Csatlakozás</button>';
            echo "</form>";
        }
        ?>
    </div>
    <div class="w3-half w3-padding">
        <h1>Tagok</h1>
        <hr>
        <div class="w3-ul">
            <?php
            $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM CSOPORT_TAGOK WHERE CSOPORT_ID = :id_bv");
            oci_bind_by_name($stid, ':id_bv', $id);
            oci_execute($stid);

            while ($row = oci_fetch_assoc($stid)) {
                $tempUser = new User($row['FELHASZNALO_ID'], $DATABASE->getInstance());
                $tempUser->to_string();
            }
            ?>
        </div>
    </div>
</div>

<?php
create_tail();
