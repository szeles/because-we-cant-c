<?php
/**
 * Created by PhpStorm.
 * User: pekar
 * Date: 2019. 03. 26.
 * Time: 8:49
 */
include_once "head.php";
session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

create_head("messages");

$USER = new User($_SESSION['loggedInUser'], $DATABASE->getInstance());
?>
<div class="w3-sidebar w3-indigo w3-padding w3-border-right	" style="width:300px;">
    <h4 class="w3-center w3-border-bottom w3-blue">Üzeneteid:</h4>
    <div class="w3-container w3-animate-opacity">
        <ul class="w3-ul w3-card">
            <?php
            foreach ($USER->getContacts() as $contact) {
                $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM felhasznalok WHERE ID = :id_bv");
                oci_bind_by_name($stid, ":id_bv", $contact);

                oci_execute($stid);

                $name = oci_fetch_assoc($stid)['NEV'];

                echo "<li class='w3-row'>
                    <a href='messages.php?id={$contact}'><span>{$name}</span></a>
                </li>";
            }
            ?>
        </ul>
    </div>
</div>

<div style="margin-left:300px;">
    <div class="display-message">
        <?php
        if (isset($_GET['id'])) {
            $user_two = $_GET['id'];
            $user_one = $_SESSION['loggedInUser'];

            $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM beszelgetesek WHERE (kitol=:user_one AND kinek=:user_two) OR (kitol=:user_two AND kinek=:user_one)");//

            oci_bind_by_name($stid, ":user_one", $user_one);
            oci_bind_by_name($stid, ":user_two", $user_two);

            oci_execute($stid);

            $row =  oci_fetch_assoc($stid);
            if ($row > 0) {
                $conversation_id =  $row['ID'];
            } else {
                $stid = oci_parse($DATABASE->__get('connection'), 'INSERT INTO beszelgetesek VALUES (0, :u_one_bv, :u_two_bv)');
                oci_bind_by_name($stid, ":u_one_bv", $user_one);
                oci_bind_by_name($stid, ":u_two_bv", $user_two);

                oci_execute($stid);

                $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM beszelgetesek WHERE (kitol=:user_one AND kinek=:user_two) OR (kitol=:user_two AND kinek=:user_one)");
                oci_bind_by_name($stid, ":user_one", $user_one);
                oci_bind_by_name($stid, ":user_two", $user_two);
                oci_execute($stid);

                $conversation_id = oci_fetch_assoc($stid)['ID'];
            }
        } else {
            echo "Válassz a listából!";
        }
        ?>
    </div>

    <?php
    if (isset($conversation_id)) {
        echo "<div class=\"w3-margin\">";
        echo "<hr>";
        echo "<input type=\"hidden\" id=\"conversation_id\" value=" . $conversation_id . ">";
        echo "<input type=\"hidden\" id=\"user_form\" value=" . $user_one . ">";
        echo "<input type=\"hidden\" id=\"user_to\" value=" . $user_two . ">";
        echo "<div>";
        echo "<input class='w3-round-xxlarge w3-padding' style='width: 30%; margin-right: 10px' type=\"text\" id=\"message\" placeholder=\"Üzenet\">";
        echo "<button class=\"w3-indigo w3-hover-yellow w3-padding w3-right\" id=\"reply\">Küldés</button>";
        echo "</div>";
        echo "</div>";
    }

    ?>

<script type="text/javascript" src="../jquery.js"></script>
<script>
    if ($("#conversation_id").val() !== undefined) {
        $(document).ready(function () {
            $("#reply").on("click", function () {
                var message = $.trim($("#message").val()),
                    conversation_id = $.trim($("#conversation_id").val()),
                    user_form = $.trim($("#user_form").val()),
                    user_to = $.trim($("#user_to").val());


                if ((message != "") && (conversation_id != "") && (user_form != "") && (user_to != "")) {
                    $.post("./functions/f_post_message.php", {
                        message: message,
                        conversation_id: conversation_id,
                        user_form: user_form,
                        user_to: user_to
                    }, function (data) {
                        $("#message").val("");
                    });
                }
            });

            let c_id = $("#conversation_id").val();
            console.log(c_id);
            setInterval(function () {
                $(".display-message").load("./functions/f_get_message.php?c_id=" + c_id);
            }, 1000);

            $(".display-message").scrollTop(1000);
        });
    }
</script>
<?php
create_tail();
