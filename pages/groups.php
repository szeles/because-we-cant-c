<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 27/04/2019
 * Time: 16:24
 */

include_once "head.php";
session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

create_head("groups");
?>
    <div class="w3-main w3-padding">
        <div class="w3-container">
            <h2>Ajánlott csoportok:</h2>
            <?php
                $stid = oci_parse($DATABASE->__get('connection'), "select csoport_id from csoport_tagok 
    where felhasznalo_id IN (select kije from ismerosok where kinek = :id_bv AND visszaigazolt = 1) and not felhasznalo_id = :id_bv");
                oci_bind_by_name($stid, ":id_bv", $_SESSION["loggedInUser"]);
                oci_execute($stid);

                if (oci_fetch_assoc($stid) > 0) {
                    oci_execute($stid);
                    while ($row = oci_fetch_assoc($stid)) {
                        $stid1 = oci_parse($DATABASE->__get('connection'), "SELECT * FROM CSOPORTOK WHERE ID = :id_bv");
                        oci_bind_by_name($stid1, ":id_bv", $row['ID']);
                        oci_execute($stid1);

                        $row1 = oci_fetch_assoc($stid1);

                        echo "<div class='w3-card w3-indigo w3-margin'>";
                        echo "<a href='group.php?id={$row1['ID']}'>{$row1['NEV']}</a>";
                        echo "</div>";
                    }
                } else {
                    echo "<p>Nincsenek ajánott csoportok.</p>";
                }
            ?>
        </div>
        <div class="w3-row">
            <h2>Csoport létrehozása</h2>
            <form method="post" action="functions/f_create_group.php">
                <input class="w3-round-xxlarge w3-padding w3-margin" name="nev" placeholder="Csoport neve" type="text"/>
                <input class="w3-round-xxlarge w3-padding w3-margin" name="leiras" placeholder="Leírás" type="text"/>
                <button class="w3-padding w3-indigo w3-hover-yellow" type="submit">Létrehozás</button>
            </form>
        </div>
        <hr>
        <div>
            <h2>Keresés</h2>
            <form method="post" action="groups.php">
                <div class="w3-margin"><input name="lookFor" class="w3-round-xxlarge w3-padding w3-margin"
                                              style="width: 30%" type="text"
                                              placeholder="  Csoport"/></div>
                <button class="w3-padding w3-indigo w3-hover-yellow" type="submit">Keresés</button>
            </form>
        </div>
    </div>
<?php
if (isset($_POST['lookFor'])) {
    $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM CSOPORTOK WHERE NEV LIKE '%" . $_POST['lookFor'] . "%' ORDER BY NEV");
    $i = $_POST['lookFor'];
    oci_execute($stid);

    if (oci_fetch_assoc($stid) > 0) {
        echo "<div class='w3-ul'> ";
        oci_execute($stid);
        while ($row = oci_fetch_assoc($stid)) {
            echo "<div class='w3-card w3-indigo w3-margin'>";
                echo "<a href='group.php?id={$row['ID']}'>{$row['NEV']}</a>";
            echo "</div>";        }
        echo "</div>";
    } else {
        echo "<h3>Nincs találat.</h3>";
    }
}
?>

<?php
create_tail();
?>