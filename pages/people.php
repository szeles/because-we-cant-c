<?php

include_once 'head.php';
session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

create_head('people');
$USER = new User($_SESSION['loggedInUser'], $DATABASE->getInstance());

?>
    <h2>Ajánlott ismerősök:</h2>
    <div class="recommendedContacts">
        <?php
        $stid = oci_parse($DATABASE->__get('connection'), "SELECT ism.kije FROM ismerosok ism
            JOIN felhasznalok fh_2 ON ism.kinek = fh_2.id
            JOIN felhasznalok fh_1 ON ism.kije = fh_1.id
            WHERE kinek IN(SELECT kije FROM ismerosok WHERE kinek = :id_user_1)
            AND kije NOT IN (SELECT kije FROM ismerosok WHERE kinek = :id_user_2) 
            AND NOT kije = :id_user_3 
            AND visszaigazolt = 1");
        oci_bind_by_name($stid, ':id_user_1', $_SESSION["loggedInUser"]);
        oci_bind_by_name($stid, ':id_user_2', $_SESSION["loggedInUser"]);
        oci_bind_by_name($stid, ':id_user_3', $_SESSION["loggedInUser"]);

        oci_execute($stid);

        while ($row = oci_fetch_assoc($stid)) {
            $u = new User($row["KIJE"], $DATABASE->getInstance());
            $u->to_string();
        }
        ?>
    </div>

    <div class="w3-main w3-padding search">
        <form method="post" action="people.php">
            <div class="w3-margin"><input name="lookFor" class="w3-round-xxlarge w3-padding w3-margin" style="width: 30%" type="text"
                                          placeholder="  Név"/></div>
            <button class="w3-padding w3-indigo w3-hover-yellow" type="submit">Keresés</button>
        </form>
        <hr>
    </div>
            <?php
            if (isset($_POST['lookFor'])) {
                $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM FELHASZNALOK WHERE NEV LIKE '%" . $_POST['lookFor'] . "%' and id != 0 ORDER BY NEV");
                $i = $_POST['lookFor'];
                oci_execute($stid);

                if (oci_fetch_assoc($stid) > 0) {
                    oci_execute($stid);
                    echo "<div class='w3-row-padding'>";
                    while ($row = oci_fetch_assoc($stid)) {
                        $tempUser = new User($row['ID'], $DATABASE->getInstance());
                        $tempUser->to_string();
                    }

                } else {
                    echo "<h3>Nincs találat.</h3>";
                }
            } else { ?>
                <div>
                    <h3>Írd be keresendő személy nevét!</h3>
                </div>
                <?php
            }
            ?>

<?php
create_tail();
?>