<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 26/03/2019
 * Time: 18:29
 * @property resource connection
 */

class Database {
    public $connection;

    private $tns = "(DESCRIPTION = 
                        (ADDRESS_LIST = 
                            (ADDRESS = 
                                (PROTOCOL = TCP)
                                (HOST = localhost)
                                (PORT = 4000)
                            )
                        )
                        (CONNECT_DATA = 
                            (SID = kabinet)
                        )
                    )";

    public function __construct() {
        $this->connection = oci_connect('H672334', 'PATpat1996@', $this->tns, 'UTF8');
        if (!$this->connection) {
            die("Sikertelen kapcsolódás!");
        }
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        } else {
            return null;
        }
    }

    public function getInstance() {
        return $this;
    }
}