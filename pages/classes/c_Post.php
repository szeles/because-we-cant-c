<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 26/03/2019
 * Time: 18:10
 */

include_once "c_Database.php";
include_once "c_User.php";

class Post {
    private $POST_ID;
    private $POST_TEXT;
    private $OWNER;
    private $POST_DATE;
    private $POST_OWNER_NAME;
    private $POST_PIC_ID;
    private $PROF_PIC_ID;
    private $comments = [];

    public function __construct($id, $__DBINSTANCE) {
        $stid = oci_parse($__DBINSTANCE->__get('connection'), "SELECT * FROM v_news_feed WHERE post_id = :id_bv");
        oci_bind_by_name($stid, ':id_bv', $id);
        oci_execute($stid);
        $this->fill_base_data(oci_fetch_assoc($stid));

        $stid = oci_parse($__DBINSTANCE->__get('connection'), "SELECT * FROM v_news_feed_comments WHERE post_id = :id_bv ORDER BY comment_date DESC");
        oci_bind_by_name($stid, ':id_bv', $id);
        oci_execute($stid);
        $this->fill_comments($stid);
    }

    private function fill_base_data($result) {
        $this->setPOSTID($result['POST_ID']);
        $this->setPOSTTEXT($result['POST_TEXT']);
        $this->setPOSTDATE($result['POST_DATE']);
        $this->setOWNER($result['OWNER']);
        $this->setPOSTOWNERNAME($result['POST_OWNER_NAME']);
        $this->setPOSTPICID($result['POST_PIC_ID']);
        $this->setPROFPICID($result['PROF_PIC_ID']);
    }

    private function fill_comments($result) {
        while ($row = oci_fetch_assoc($result)) {
            array_push($this->comments, $row);
        }
    }

    public function to_string() {
        echo '<div class="post" >';
        echo '    <div class="imageHolder">';
        echo '        <img class="ppic" src="images/' . $this->getPROFPICID() . '.jpg">';
        echo '    </div>';
        echo '    <div class="contentHolder">';
        echo '        <h1><a href="profile.php?id=' . $this->getOWNER() . '">'. $this->getPOSTOWNERNAME() . '</a></h1>';
        echo '        <p>' . $this->getPOSTTEXT() . '</p>';
        echo '        <br>';

        if ($this->getPOSTPICID() != null):
            echo '        <img style="object-fit: cover;" width="512" height="512" src="images/' . $this->getPOSTPICID() . '.jpg">';
        endif;

        echo '        <br>';
        echo '        <p><span class="comments" onclick="showComments('. $this->getPOSTID() .')">Kommentek</span>' . '<span class="date">' . $this->getPOSTDATE() . '</span></p>';
        echo '    </div>';
        echo '</div>';
        echo '<div class="commentSection" id="' . $this->getPOSTID() . '">';
        echo '<br>';
        if (sizeof($this->comments) > 0) {
            foreach ($this->comments as $comment) {
                echo '<div class="comment">';
                echo '    <div class="commentPicture">';
                echo '        <img src="images/' . $comment['COMMENT_PROFPIC_ID'] . '.jpg">';
                echo '    </div>';
                echo '    <div class="commentContent">';
                echo '        <h3><a href="profile.php?id=' . $comment['OWNER'] . '">' . $comment['COMMENT_OWNER_NAME'] . '</a></h3>';
                echo '        <p>' . $comment['COMMENT_TEXT'] . '</p>';
                echo '        <p>' . $comment['COMMENT_DATE'] . '</p>';
                echo '    </div>';
                echo '</div>';
                echo '<hr>';

            }
        }
        echo '<div class="createPost">';
        echo '<form method="post" action="functions/f_comment.php">';
        echo '      <br>';
        echo '      <input type="hidden" value="'. $this->getPOSTID() . '" name="post_id">';
        echo '      <label>';
        echo '            <input type="text" name="post_text">';
        echo '      </label>';
        echo '      <br>';
        echo '        <button type="submit" id="postButton' . $this->getPOSTID() . '">Hozzászólás</button>';
        echo '    </form>';
        echo '</div>';
        echo '<br>';
        echo '</div>';


    }

    /**
     * @return mixed
     */
    public function getPOSTID()
    {
        return $this->POST_ID;
    }

    /**
     * @param mixed $POST_ID
     */
    public function setPOSTID($POST_ID)
    {
        $this->POST_ID = $POST_ID;
    }

    /**
     * @return mixed
     */
    public function getPOSTTEXT()
    {
        return $this->POST_TEXT;
    }

    /**
     * @param mixed $POST_TEXT
     */
    public function setPOSTTEXT($POST_TEXT)
    {
        $this->POST_TEXT = $POST_TEXT;
    }

    /**
     * @return mixed
     */
    public function getOWNER()
    {
        return $this->OWNER;
    }

    /**
     * @param mixed $OWNER
     */
    public function setOWNER($OWNER)
    {
        $this->OWNER = $OWNER;
    }

    /**
     * @return mixed
     */
    public function getPOSTDATE()
    {
        return $this->POST_DATE;
    }

    /**
     * @param mixed $POST_DATE
     */
    public function setPOSTDATE($POST_DATE)
    {
        $this->POST_DATE = $POST_DATE;
    }

    /**
     * @return mixed
     */
    public function getPOSTOWNERNAME()
    {
        return $this->POST_OWNER_NAME;
    }

    /**
     * @param mixed $POST_OWNER_NAME
     */
    public function setPOSTOWNERNAME($POST_OWNER_NAME)
    {
        $this->POST_OWNER_NAME = $POST_OWNER_NAME;
    }

    /**
     * @return mixed
     */
    public function getPOSTPICID()
    {
        return $this->POST_PIC_ID;
    }

    /**
     * @param mixed $POST_PIC_ID
     */
    public function setPOSTPICID($POST_PIC_ID)
    {
        $this->POST_PIC_ID = $POST_PIC_ID;
    }

    /**
     * @return mixed
     */
    public function getPROFPICID()
    {
        return $this->PROF_PIC_ID;
    }

    /**
     * @param mixed $PROF_PIC_ID
     */
    public function setPROFPICID($PROF_PIC_ID)
    {
        $this->PROF_PIC_ID = $PROF_PIC_ID;
    }

    /**
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param array $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }


}
