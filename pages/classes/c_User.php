<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 26/03/2019
 * Time: 18:07
 */
include_once "c_Database.php";

class User {
    private $__DBINSTANCE;

    private $id;
    private $password;
    private $userName;
    private $name;
    private $profilePicture;
    private $coverPicture;
    private $birthDay;
    private $contacts = [];
    private $images = [];
    private $groups = [];

    public function __construct($id, $__DBINSTANCE) {
        $this->__DBINSTANCE = $__DBINSTANCE;
        $stid = oci_parse($__DBINSTANCE->__get('connection'), "SELECT * FROM FELHASZNALOK WHERE ID = :id_bv");
        oci_bind_by_name($stid, ":id_bv", $id);
        oci_execute($stid);
        $this->setDataFromQuery(oci_fetch_assoc($stid));

        $stid = oci_parse($__DBINSTANCE->__get('connection'), "SELECT * FROM ISMEROSOK WHERE kinek = :id_bv");
        oci_bind_by_name($stid, ":id_bv", $id);
        oci_execute($stid);
        $this->fillContacts($stid);

        $stid = oci_parse($__DBINSTANCE->__get('connection'), "SELECT * FROM KEPEK WHERE FELHASZNALO = :id_bv");
        oci_bind_by_name($stid, ":id_bv", $id);
        oci_execute($stid);
        $this->fillImages($stid, $__DBINSTANCE);

        $stid = oci_parse($__DBINSTANCE->__get('connection'), "SELECT * FROM CSOPORT_TAGOK WHERE FELHASZNALO_ID = :id_bv");
        oci_bind_by_name($stid, ":id_bv", $id);
        oci_execute($stid);
        $this->fillGroups($stid);
    }

    public function getImages() {
        return $this->images;
    }

    private function setDataFromQuery($row) {
        $this->setId($row['ID']);
        $this->setUserName($row['FELHASZNALONEV']);
        $this->setName($row['NEV']);
        $this->setProfilePicture($row['PROFILKEP_ID']);
        $this->setCoverPicture($row['BORITOKEP_ID']);
        $this->setBirthDay($row['SZULETESNAP']);
    }

    private function fillContacts($result) {
        while ($row = oci_fetch_assoc($result)) {
            if ($row['VISSZAIGAZOLT'] == 1) {
                if ($row['KINEK'] == $this->getId()) {
                    array_push($this->contacts, $row['KIJE']);
                } else {
                    array_push($this->contacts, $row['KINEK']);
                }
            }
        }
    }

    public function to_string() {
        echo '<div class="user_wrap w3-card">';
            echo "<img class='w3-circle w3-margin' width='200px' src='images/{$this->getProfilePicture()}.jpg' />";
            echo "<div>";
                echo "<h3><a style='text-decoration: none' href='profile.php?id={$this->getId()}'>{$this->getName()}</a></h3>";
            echo "</div>";
            if (!in_array($this->getId(), $this->getContacts()) and $this->getId() != $_SESSION['loggedInUser']) {
                echo "<form method='post' action='functions/f_contact.php'>
                    <input type='hidden' value='{$this->getId()}' name='new_contact'>";
                if (basename($_SERVER['PHP_SELF'] == "/because-we-cant-c/pages/contacts.php")) {
                    echo "<a href='functions/f_confirm_friend_request.php?id={$this->getId()}'>Visszaigazol</a>";
                    echo "<br>";
                } else {
                    echo "<button class='w3-hover-grey w3-margin w3-yellow w3-padding' type='submit'>Ismerősnek jelöl</button>";
                }
                echo "</form>";
            } else echo "<div class='w3-indigo w3-large w3-margin w3-padding'>Ismerősök vagytok!</div>";
        echo "</div>";
    }

    private function fillImages($result, $__DBINSTANCE) {
        while ($row = oci_fetch_assoc($result)) {
            $stid = oci_parse($__DBINSTANCE->__get('connection'), "SELECT leiras FROM kepek WHERE id = :image_id_bv");
            oci_bind_by_name($stid, ':image_id_bv', $row['ID']);
            oci_execute($stid);
            array_push($this->images, ['IMAGE_ID' => $row['ID'], 'IMAGE_DESC' => oci_fetch_assoc($stid)['LEIRAS']]);
        }
    }

    private function fillGroups($result) {
        while ($row = oci_fetch_assoc($result)) {
            array_push($this->groups, $row['CSOPORT_ID']);
        }
    }

    /**
     * @return mixed
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * @param mixed $profilePicture
     */
    public function setProfilePicture($profilePicture)
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * @return mixed
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * @param mixed $birthDay
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;
    }

    /**
     * @return mixed
     */
    public function getCoverPicture()
    {
        return $this->coverPicture;
    }

    /**
     * @param mixed $coverPicture
     */
    public function setCoverPicture($coverPicture)
    {
        $this->coverPicture = $coverPicture;
    }

    /**
     * @return array
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param array $contacts
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param array $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }
}