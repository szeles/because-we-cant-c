<?php
/**
 * Created by PhpStorm.
 * User: pekar
 * Date: 2019. 03. 06.
 * Time: 16:08
 */
include 'head.php';
create_head('login');
?>
<script>
    console.log(LABELS._LANG);
</script>
<body onload="configureLabels()">
    <br>
    <h1 class="mainContent">SmooZe</h1>
    <svg class="icon"><use xlink:href="#icon-bubbles3"></use>
        <symbol id="icon-bubbles3" viewBox="0 0 36 32">
            <title>bubbles3</title>
            <path d="M34 28.161c0 1.422 0.813 2.653 2 3.256v0.498c-0.332 0.045-0.671 0.070-1.016 0.070-2.125 0-4.042-0.892-5.398-2.321-0.819 0.218-1.688 0.336-2.587 0.336-4.971 0-9-3.582-9-8s4.029-8 9-8c4.971 0 9 3.582 9 8 0 1.73-0.618 3.331-1.667 4.64-0.213 0.463-0.333 0.979-0.333 1.522zM7.209 6.912c-2.069 1.681-3.209 3.843-3.209 6.088 0 1.259 0.35 2.481 1.039 3.63 0.711 1.185 1.781 2.268 3.093 3.133 0.949 0.625 1.587 1.623 1.755 2.747 0.056 0.375 0.091 0.753 0.105 1.129 0.233-0.194 0.461-0.401 0.684-0.624 0.755-0.755 1.774-1.172 2.828-1.172 0.168 0 0.336 0.011 0.505 0.032 0.655 0.083 1.323 0.125 1.987 0.126v4c-0.848-0-1.68-0.054-2.492-0.158-3.437 3.437-7.539 4.053-11.505 4.144v-0.841c2.142-1.049 4-2.961 4-5.145 0-0.305-0.024-0.604-0.068-0.897-3.619-2.383-5.932-6.024-5.932-10.103 0-7.18 7.163-13 16-13 8.702 0 15.781 5.644 15.995 12.672-1.284-0.572-2.683-0.919-4.133-1.018-0.36-1.752-1.419-3.401-3.070-4.742-1.104-0.897-2.404-1.606-3.863-2.108-1.553-0.534-3.211-0.804-4.928-0.804s-3.375 0.271-4.928 0.804c-1.46 0.502-2.76 1.211-3.863 2.108z"></path>
        </symbol>
    </svg>
    <h2 id="test">Üdvözlünk a weboldalon!<!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport"
                  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
        </head>
        <body>
        
        </body>
        </html></h2>
    <br>
    <p style="cursor: pointer" onclick="showPopup('loginPopup')" id="login">Bejelentkezés</p>
    <br>
    <p style="cursor: pointer" onclick="showPopup('registerPopup')"id="register">Regisztráció</p>

    <div id="loginPopup" class="hidden">
        <p class="closeButton" onclick="hidePopup('loginPopup')">+</p>
        <h2 style="float: left; margin-left: 5%;" id="login1">Bejelentkezés</h2>
        <form method="POST" action="functions/f_login.php">
            <br><br><br><br>
            <p id="userNameLabel">Felhasználónév</p>
            <input type="text" name="username" tabindex="1" required>
            <br>
            <p id="passwordLabel">Jelszó</p>
            <input type="password" name="password" tabindex="2" required>
            <br><br><br>
            <button type="submit" name="submit" tabindex="3" id="login2">Bejelentkezés</button><br>
        </form>
    </div>

    <div id="registerPopup" class="hidden">
        <p class="closeButton" onclick="hidePopup('registerPopup')">+</p>
        <h2 style="float: left; margin-left: 5%;" id="register1">Regisztráció</h2>
        <form method="POST" action="functions/f_register.php">
            <br><br><br><br>
            <p id="registerLabel1">Felhasználónév</p>
            <input type="text" name="username" tabindex="1" required><br>
            <p id="registerLabel2">Jelszó</p>
            <input type="password" name="jelszo" tabindex="2" required><br>
            <p id="registerLabel3">Teljes név</p>
            <input type="text" name="nev" tabindex="3" required><br>
            <p id="registerLabel4">Születésnap</p>
            <input type="date" name="szuletesnap" tabindex="4" required><br>
            <br><br>
            <button type="submit" name="submit" tabindex="5" id="register2">Regisztráció</button><br>
        </form>
    </div>
    <footer class="loginFooter">
        <br>
        <p>SmooZe Social App. Készítették: Pekár Patrik (YZNK0B), és Szeles László (UW2LJX).</p>
        <br>
        <p>"Minden jog fenntartva"</p>
    </footer>
</body>
<script>
    function popupCheck(windowName) {
        let otherWindow = windowName === 'loginPopup' ? 'registerPopup' : 'loginPopup';
        return document.getElementById(otherWindow).classList.contains('visible');
    }

    function showPopup(windowName) {
        let otherWindow = windowName === 'loginPopup' ? 'registerPopup' : 'loginPopup';
        if (popupCheck(windowName)) {
            hidePopup(otherWindow);
        } else {
            hidePopup(windowName);
        }
        document.getElementById(windowName).classList.remove('hidden');
        document.getElementById(windowName).classList.add('visible');
    }

    function hidePopup(windowName) {
        document.getElementById(windowName).classList.add('hidden');
        document.getElementById(windowName).classList.remove('visible');
    }

</script>


