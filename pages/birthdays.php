<?php

include_once "head.php";
session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

create_head("birthdays");
?>
    <div class="w3-main">
        <div>
            <h1>Születésnapok a hónapban:</h1>
        </div>
        <div class="w3-container">
            <?php
            $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM FELHASZNALOK 
                                                        WHERE to_char(SYSDATE, 'MONTH') = to_char(SZULETESNAP, 'MONTH') AND ID != 0");
            oci_execute($stid);

            while ($row = oci_fetch_assoc($stid)) {
                $tempUser = new User($row['ID'], $DATABASE->getInstance());
                $tempUser->to_string();
            }

            ?>
        </div>
    </div>
<?php
create_tail();
