<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 27/04/2019
 * Time: 15:58
 */

include_once "head.php";
session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

create_head("news_feed");
?>
    <div class="posts">
        <div class="createPost">
            <br>
            <form method="post" action="functions/f_post.php">
                <input placeholder="Mi jár a fejedben?" class="w3-padding" type="text" name="post_text">
                <button class="w3-margin w3-indigo w3-hover-yellow w3-padding" type="submit" id="postButton">Posztolás</button>
            </form>
        </div>
        <br><br>

        <?php
            $stid = oci_parse($DATABASE->__get('connection'), "SELECT POST_ID FROM v_news_feed WHERE OWNER IN (select kije from ismerosok where kinek = :user_bv and visszaigazolt = 1) OR OWNER = :self_bv ORDER BY post_date DESC");
            oci_bind_by_name($stid, ':user_bv', $_SESSION["loggedInUser"]);
            oci_bind_by_name($stid, ':self_bv', $_SESSION["loggedInUser"]);
            oci_execute($stid);
            if (oci_fetch_assoc($stid) > 0) {
                oci_execute($stid);
                while ($row = oci_fetch_assoc($stid)) {
                    $p = new Post($row['POST_ID'], $DATABASE->getInstance());
                    $p->to_string();
                }
            } else {
                echo "<br><div>Nincsenek posztjaid</div>";
            }?>
        <br>
    </div>

    <script>
        function showComments(id) {
            if (document.getElementById(id).style.display !== "block") {
                document.getElementById(id).style.display = "block";
            } else {
                document.getElementById(id).style.display = "none";
            }
        }
    </script>

<?php
create_tail();