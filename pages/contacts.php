<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 27/04/2019
 * Time: 16:24
 */

include_once "head.php";
session_start();

if (empty($_SESSION['loggedInUser'])) {
    die("Nincs bejelentkezett felhasználó.");
}

$USER = new User($_SESSION['loggedInUser'], $DATABASE->getInstance());

create_head("contacts");
?>
    <div class="w3-main">
        <div class="w3-container">
            <div class="w3-half">
                <div class="w3-container">
                    <h3>Ismerőseid:</h3>
                    <div>
                        <?php
                        foreach ($USER->getContacts() as $c) {
                            $tempUser = new User($c, $DATABASE->getInstance());
                            $tempUser->to_string();
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="w3-half">
                <div class="w3-container">
                    <h3>Ismerősnek jelöltek:</h3>
                    <div>
                        <?php
                        $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM ismerosok WHERE KIJE = :id_bv AND visszaigazolt = 0");
                        oci_bind_by_name($stid, ":id_bv", $_SESSION['loggedInUser']);
                        oci_execute($stid);

                        while ($row = oci_fetch_assoc($stid)) {
                            echo "<div>";
                            $tempUser = new User($row['KINEK'], $DATABASE->getInstance());
                            $tempUser->to_string();
                            echo "</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
create_tail();
