<?php
/**
 * Created by PhpStorm.
 * User: pekar
 * Date: 2019. 03. 04.
 * Time: 19:55
 */
include_once "classes/c_Database.php";
include_once "classes/c_User.php";
include_once "classes/c_Post.php";

$DATABASE = new Database();
function create_head($page_tittle) {
    ?>
        <!DOCTYPE html>
        <html lang="hu">
        <head>
            <meta charset="utf-8">
            <title>SmooZe</title>
            <link rel="shortcut icon" type="image/x-icon" href="images/smooze_logo.png"/>
            <link rel="stylesheet" href="style/style.css">
            <link rel="stylesheet" href="style/<?php echo basename($_SERVER['PHP_SELF'], '.php'); ?>.css">
            <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        </head>
        <body>
        <?php if($page_tittle != 'login') { ?>
            <ul>
                <li><a <?php if ($page_tittle == "news_feed") { echo "class='active'"; }?> href="news_feed.php" id="menu">Főoldal</a></li>
                <li><a <?php if ($page_tittle == $_SESSION["loggedInUser"]) { echo "class='active'"; }?> href="profile.php?id=<?php echo $_SESSION["loggedInUser"]; ?>" id="menu1">Profilom</a></li>
                <li><a <?php if ($page_tittle == "messages") { echo "class='active'"; }?> href="messages.php" id="menu2">Üzenetek</a></li>
                <li><a <?php if ($page_tittle == "contacts") { echo "class='active'"; }?> href="contacts.php" id="menu3">Ismerősök</a></li>
                <li><a <?php if ($page_tittle == "groups") { echo "class='active'"; }?> href="groups.php" id="menu4">Csoportok</a></li>
                <li><a <?php if ($page_tittle == "birthdays") { echo "class='active'"; }?> href="birthdays.php" id="menu6">Születésnapok</a></li>
                <li><a <?php if ($page_tittle == "people") { echo "class='active'"; }?> href="people.php" id="menu7">Emberek</a></li>
                <li style="float:right;"><a href="functions/f_logout.php" id="menu7">Kijelentkezés</a></li>
            </ul>
        <?php }
}

function create_tail() {
    ?>
    </body>
</html>
<?php
}
?>





