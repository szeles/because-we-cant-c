<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 27/04/2019
 * Time: 16:32
 */

include_once "../head.php";

session_start();
session_unset();
session_destroy();

header('location: ../login.php');