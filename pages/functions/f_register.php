<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 27/04/2019
 * Time: 15:50
 */

include_once "../head.php";

$username = $_POST['username'];
$password = $_POST['jelszo'];
$name = $_POST['nev'];
$birthday = $_POST['szuletesnap'];

$stid = oci_parse($DATABASE->__get('connection'), "
INSERT INTO felhasznalok VALUES (123, :u_bv, :p_bv, :n_bv, 0, 0,TO_DATE(:b_bv,'YYYY-MM-DD'))
");

oci_bind_by_name($stid, ":u_bv", $username);
oci_bind_by_name($stid, ":p_bv", $password);
oci_bind_by_name($stid, ":n_bv", $name);
oci_bind_by_name($stid, ":b_bv", $birthday);

if (oci_execute($stid)){
    header("Location: ../login.php");
} else {
    die("Sikertelen regisztráció");
}