<?php
require_once("../head.php");
session_start();

$stid = oci_parse($DATABASE->__get('connection'), "
        UPDATE ismerosok SET visszaigazolt = 1 WHERE kinek = :friend_bv AND kije = :own_bv");
$stid1 = oci_parse($DATABASE->__get('connection'), "
        INSERT INTO ismerosok VALUES (:own_bv, :friend_bv, 1)");

oci_bind_by_name($stid, ":friend_bv", $_GET['id']);
oci_bind_by_name($stid, ":own_bv", $_SESSION['loggedInUser']);

oci_bind_by_name($stid1, ":friend_bv", $_GET['id']);
oci_bind_by_name($stid1, ":own_bv", $_SESSION['loggedInUser']);

oci_execute($stid);
oci_execute($stid1);

header("Location: ../contacts.php");

?>