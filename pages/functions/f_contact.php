<?php

include_once '../head.php';
session_start();

$stid = oci_parse($DATABASE->__get('connection'), "INSERT INTO ISMEROSOK VALUES (:own_bv, :another_bv, 0)");

oci_bind_by_name($stid, ":own_bv", $_SESSION['loggedInUser']);
oci_bind_by_name($stid, ":another_bv", $_POST['new_contact']);

if (oci_execute($stid)) {
    header('Location: ../news_feed.php');
} else {
    die('Sikertelen hozzáadás');
}

