<?php
require_once("../head.php");

$stid = oci_parse($DATABASE->__get('connection'), "
        INSERT INTO uzenetek VALUES (1, :conv_id_bv, :kitol_bv,:kinek_bv, :msg_bv, SYSDATE)");

oci_bind_by_name($stid, ":conv_id_bv", $_POST['conversation_id']);
oci_bind_by_name($stid, ":kitol_bv", $_POST['user_form']);
oci_bind_by_name($stid, ":kinek_bv", $_POST['user_to']);
oci_bind_by_name($stid, ":msg_bv", $_POST['message']);

oci_execute($stid);