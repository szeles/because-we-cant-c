<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 28/04/2019
 * Time: 22:32
 */

include_once "../head.php";
session_start();


$stid = oci_parse($DATABASE->__get('connection'), "INSERT INTO hozzaszolasok VALUES (:id_bv, SYSDATE, :post_bv, :text_bv)");
oci_bind_by_name($stid, ':id_bv', $_SESSION['loggedInUser']);
oci_bind_by_name($stid, ':post_bv', $_POST['post_id']);
oci_bind_by_name($stid, ':text_bv', $_POST['post_text']);
oci_execute($stid);

header('Location: ../news_feed.php');