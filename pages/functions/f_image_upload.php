<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 11/05/2019
 * Time: 13:40
 */

include_once "../head.php";
session_start();

if(isset($_FILES['imageToUpload'])){
    $errors= array();
    $file_name = $_FILES['imageToUpload']['name'];
    $file_size =$_FILES['imageToUpload']['size'];
    $file_tmp =$_FILES['imageToUpload']['tmp_name'];
    $file_type=$_FILES['imageToUpload']['type'];

    if(empty($errors)==true){
        $query = oci_parse($DATABASE->__get('connection'), "INSERT INTO kepek VALUES (0, :id_user, :desc_bv)");
        oci_bind_by_name($query, ':id_user', $_SESSION["loggedInUser"]);
        oci_bind_by_name($query, ':desc_bv', $_POST["description"]);
        oci_execute($query);

        $query = oci_parse($DATABASE->__get('connection'),"SELECT MAX(id) AS MAX_KEP_ID FROM kepek");
        oci_execute($query);
        $id_to_set = oci_fetch_assoc($query)["MAX_KEP_ID"];

        move_uploaded_file($file_tmp,"../images/". $id_to_set .'.jpg');
        header('Location: ../profile.php?id=' . $_SESSION["loggedInUser"]);
    }else{
        print_r($errors);
    }
}
?>