<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 26/03/2019
 * Time: 20:09
 */

include_once "../head.php";
session_start();

$stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM FELHASZNALOK WHERE felhasznalonev = :fn_bv AND jelszo = :jsz_bv");

oci_bind_by_name($stid, ":fn_bv", $_POST['username']);
oci_bind_by_name($stid, ":jsz_bv", $_POST['password']);

oci_execute($stid);

$row = oci_fetch_assoc($stid);

if ($row > 0) {
    if (empty($_SESSION['loggedInUser'])) {
        $_SESSION["loggedInUser"] = $row['ID'];
        echo $_SESSION['loggedInUser'];
        header("Location: ../news_feed.php");
    } else die("Már van bejelentkezett felhasználó.");
} else {
    echo "Sikertelen bejelentkezés.";
}
