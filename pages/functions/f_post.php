<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 27/04/2019
 * Time: 23:26
 */

include_once "../head.php";

session_start();


$stid = oci_parse($DATABASE->__get('connection'), "INSERT INTO POSZTOK VALUES (null, :session_bv, :text_bv, SYSDATE, null)");

oci_bind_by_name($stid, ":session_bv", $_SESSION['loggedInUser']);
oci_bind_by_name($stid, ":text_bv", $_POST['post_text']);

oci_execute($stid);

header("Location: ../news_feed.php");