<?php

include_once "../head.php";
session_start();

$stid = oci_parse($DATABASE->__get('connection'), "INSERT INTO CSOPORTOK VALUES (1, :nev_bv, :leiras_bv, :id_bv)");

oci_bind_by_name($stid, ":nev_bv", $_POST["nev"]);
oci_bind_by_name($stid, ":leiras_bv", $_POST["leiras"]);
oci_bind_by_name($stid, ":id_bv", $_SESSION["loggedInUser"]);

oci_execute($stid);

$query = oci_parse($DATABASE->__get('connection'),"SELECT MAX(id) AS MAX_CSOPORT_ID FROM CSOPORTOK");
oci_execute($query);
$id_to_set = oci_fetch_assoc($query)["MAX_CSOPORT_ID"];

$stid = oci_parse($DATABASE->__get('connection'), "INSERT INTO CSOPORT_TAGOK VALUES (:gr_bv, :me_bv)");

oci_bind_by_name($stid, ":gr_bv", $id_to_set);
oci_bind_by_name($stid, ":me_bv", $_SESSION["loggedInUser"]);

oci_execute($stid);

header("Location: ../groups.php");