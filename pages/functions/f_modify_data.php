<?php

include_once "../head.php";
session_start();

$stid = oci_parse($DATABASE->__get('connection'), "UPDATE FELHASZNALOK SET felhasznalonev = :fn_bv,
                                                                                            jelszo = :pw_bv,
                                                                                            nev = :nm_bv,
                                                                                            szuletesnap = TO_DATE(:bd_bv,'YYYY-MM-DD')
                                                                                            WHERE id = :id_bv");

oci_bind_by_name($stid, ":fn_bv", $_POST["updatedUsername"]);
oci_bind_by_name($stid, ":pw_bv", $_POST["updatedPassword"]);
oci_bind_by_name($stid, ":nm_bv", $_POST["updatedName"]);
oci_bind_by_name($stid, ":bd_bv", $_POST['updatedBirthday']);
oci_bind_by_name($stid, ":id_bv", $_SESSION["loggedInUser"]);

oci_execute($stid);

header("Location: ../news_feed.php");