<?php

include_once "../head.php";
session_start();

$stid = oci_parse($DATABASE->__get('connection'), "INSERT INTO CSOPORT_TAGOK VALUES (:gr_bv, :me_bv)");

oci_bind_by_name($stid, ":gr_bv", $_POST["csopnev"]);
oci_bind_by_name($stid, ":me_bv", $_SESSION["loggedInUser"]);

oci_execute($stid);

header("Location: ../groups.php");