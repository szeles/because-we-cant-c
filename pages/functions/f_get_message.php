<?php

require_once("../head.php");
session_start();

if (isset($_GET['c_id'])) {
    $conversation_id = $_GET['c_id'];

    $stid = oci_parse($DATABASE->__get('connection'), "SELECT * FROM uzenetek WHERE beszelgetes_ID = :conv_id ORDER BY IDOPONT");
    oci_bind_by_name($stid, ":conv_id", $conversation_id);
    oci_execute($stid);


    while ($row = oci_fetch_assoc($stid)) {
        $user_form = $row['KITOL'];
        $message = $row['SZOVEG'];
        $time = $row['IDOPONT'];

        $stid1 = oci_parse($DATABASE->__get('connection'), "SELECT * FROM FELHASZNALOK WHERE ID = :id_bv");
        oci_bind_by_name($stid1, ':id_bv', $user_form);
        oci_execute($stid1);

        $row1 = oci_fetch_assoc($stid1);

        $my_message = $row1['ID'] == $_SESSION['loggedInUser'] ? 'right' : 'left';

        echo "<div class='w3-row'>";
            echo "<div class='w3-white w3-round w3-padding w3-margin w3-{$my_message}'>";
                echo "<div>";
                    echo "<a href='./profile.php?id={$row1['ID']}'>" . $row1['NEV'] . "</a>";
                echo "</div>";
                    echo "<div>";
                    echo "<p>".$message."</p>";
                echo "</div>";
                    echo "<p style='font-size: 10px'>".$time."</p>";
            echo "</div>";
        echo "</div>";
    }
}
?>