<?php
/**
 * Created by PhpStorm.
 * User: Szeles László
 * Date: 13/05/2019
 * Time: 17:16
 */

include_once "../head.php";
session_start();

$stid = oci_parse($DATABASE->__get('connection'), "UPDATE felhasznalok SET profilkep_id = :new_image_bv WHERE id = :id_bv");
oci_bind_by_name($stid, ':new_image_bv', $_POST['picture']);
oci_bind_by_name($stid, ':id_bv', $_SESSION['loggedInUser']);

oci_execute($stid);
header('Location: ../profile.php?id=' . $_SESSION['loggedInUser']);