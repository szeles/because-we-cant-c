CREATE TABLE felhasznalok (
	ID NUMBER,
	felhasznalonev VARCHAR(64),
	jelszo VARCHAR(64),
	nev VARCHAR(64),
	profilkep_id NUMBER DEFAULT 0,
	boritokep_id NUMBER DEFAULT 0,
	szuletesnap DATE,
	primary key (ID)
);

INSERT INTO felhasznalok VALUES (0, 'dummy', 'dummy', 'Dummy User', 0, 0, SYSDATE);

CREATE TABLE kepek (
    id NUMBER DEFAULT NULL,
    felhasznalo NUMBER,
    leiras VARCHAR(128),
    PRIMARY KEY(id)
);

INSERT INTO kepek VALUES (0, 0, '');

CREATE TABLE beszelgetesek (
	id NUMBER,
	kitol NUMBER,
	kinek NUMBER,
	PRIMARY KEY (id)
);

CREATE TABLE uzenetek (
	id NUMBER,
	beszelgetes_id NUMBER,
	kitol NUMBER,
	kinek NUMBER,
	szoveg VARCHAR(256),
	idopont DATE,
	PRIMARY KEY(id)
);

CREATE TABLE posztok (
    id NUMBER,
    felhasznalo_id NUMBER,
    szoveg VARCHAR(256),
    idopont DATE,
    kep_id NUMBER DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE ismerosok (
    kinek NUMBER,
    kije NUMBER,
    visszaigazolt NUMBER(1)
);

CREATE TABLE csoportok (
    id NUMBER,
    nev VARCHAR(128),
    leiras VARCHAR(256),
    admin_id NUMBER,
    PRIMARY KEY (id)
);

CREATE TABLE csoport_tagok(
    csoport_id NUMBER,
    felhasznalo_id NUMBER
);

CREATE TABLE hozzaszolasok (
    ki NUMBER,
    mikor DATE,
    mihez NUMBER,
    szoveg VARCHAR(256)
);

/*
VIEW DEFINÍCIÓK
*/

CREATE OR REPLACE FORCE VIEW v_news_feed AS
  SELECT
    POSZTOK.ID as POST_ID,
    POSZTOK.SZOVEG as POST_TEXT,
    POSZTOK.FELHASZNALO_ID as OWNER,
    POSZTOK.IDOPONT as POST_DATE,
    FELHASZNALOK.NEV as POST_OWNER_NAME,
    KEPEK.ID as POST_PIC_ID,
    FELHASZNALOK.PROFILKEP_ID as PROF_PIC_ID
FROM POSZTOK
JOIN FELHASZNALOK ON POSZTOK.FELHASZNALO_ID = FELHASZNALOK.ID
LEFT JOIN KEPEK ON POSZTOK.KEP_ID = KEPEK.ID;

CREATE OR REPLACE VIEW v_news_feed_comments AS
SELECT
    HOZZASZOLASOK.SZOVEG as COMMENT_TEXT,
    HOZZASZOLASOK.MIKOR as COMMENT_DATE,
    HOZZASZOLASOK.MIHEZ as POST_ID,
    FELHASZNALOK.ID as OWNER,
    FELHASZNALOK.NEV as COMMENT_OWNER_NAME,
    FELHASZNALOK.PROFILKEP_ID as COMMENT_PROFPIC_ID
FROM HOZZASZOLASOK
JOIN FELHASZNALOK ON HOZZASZOLASOK.KI = FELHASZNALOK.ID;

/*
ALTER TABLE DEFINÍCIÓK!
*/
ALTER TABLE felhasznalok ADD CONSTRAINT felhasznalok_fk_1 FOREIGN KEY (profilkep_id) REFERENCES kepek(id);
ALTER TABLE felhasznalok ADD CONSTRAINT felhasznalok_fk_2 FOREIGN KEY (boritokep_id) REFERENCES kepek(id);
ALTER TABLE felhasznalok ADD CONSTRAINT felhasznalok_uk_1 UNIQUE (felhasznalonev);

ALTER TABLE kepek ADD CONSTRAINT kepek_fk_1 FOREIGN KEY (felhasznalo) REFERENCES felhasznalok(id);

ALTER TABLE beszelgetesek ADD CONSTRAINT  beszelgetesek_fk_1 FOREIGN KEY (kitol) REFERENCES felhasznalok(id);
ALTER TABLE beszelgetesek ADD CONSTRAINT  beszelgetesek_fk_2 FOREIGN KEY (kinek) REFERENCES felhasznalok(id);

ALTER TABLE uzenetek ADD CONSTRAINT  uzenetek_fk_1 FOREIGN KEY (kitol) REFERENCES felhasznalok(id);
ALTER TABLE uzenetek ADD CONSTRAINT  uzenetek_fk_2 FOREIGN KEY (kinek) REFERENCES felhasznalok(id);
ALTER TABLE uzenetek ADD CONSTRAINT  uzenetek_fk_3 FOREIGN KEY (beszelgetes_id) REFERENCES beszelgetesek(id);

ALTER TABLE posztok ADD CONSTRAINT posztok_fk_1 FOREIGN KEY (felhasznalo_id) REFERENCES felhasznalok(id);
ALTER TABLE posztok ADD CONSTRAINT posztok_fk_2 FOREIGN KEY (kep_id) REFERENCES kepek(id);

ALTER TABLE ismerosok ADD CONSTRAINT ismerosok_fk_1 FOREIGN KEY (kinek) REFERENCES felhasznalok(id);
ALTER TABLE ismerosok ADD CONSTRAINT ismerosok_fk_2 FOREIGN KEY (kije) REFERENCES felhasznalok(id);
ALTER TABLE ismerosok ADD CONSTRAINT ismerosok_uk_1 UNIQUE (kinek, kije);

ALTER TABLE csoportok ADD CONSTRAINT csoportok_fk_1 FOREIGN KEY (admin_id) REFERENCES felhasznalok(id);
ALTER TABLE csoportok ADD CONSTRAINT csoportok_uk_1 UNIQUE (nev);

ALTER TABLE csoport_tagok ADD CONSTRAINT csoport_tagok_fk_1 FOREIGN KEY (csoport_id) REFERENCES csoportok(id);
ALTER TABLE csoport_tagok ADD CONSTRAINT csoport_tagok_fk_2 FOREIGN KEY (felhasznalo_id) REFERENCES felhasznalok(id);

ALTER TABLE hozzaszolasok ADD CONSTRAINT hosszaszolasok_fk_1 FOREIGN KEY (ki) REFERENCES felhasznalok(id);
ALTER TABLE hozzaszolasok ADD CONSTRAINT hosszaszolasok_fk_2 FOREIGN KEY (mihez) REFERENCES posztok(id);
/*
SEQUENCE DEFINÍCIÓK!
*/
CREATE SEQUENCE beszelgetesek_pk
    MINVALUE 0
    START with 0
    INCREMENT by 1
    NOMAXVALUE;

CREATE SEQUENCE	felhasznalok_pk
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1
    NOMAXVALUE;

CREATE SEQUENCE msg_pk
    MINVALUE 0
    START with 0
    INCREMENT by 1
    NOMAXVALUE;

CREATE SEQUENCE kepek_pk
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1
    NOMAXVALUE;

CREATE SEQUENCE posztok_pk
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    NOMAXVALUE;

CREATE SEQUENCE csoportok_pk
    MINVALUE 0
    START WITH 0
    INCREMENT BY 1
    NOMAXVALUE;
/*
TRIGGER DEFINÍCIÓK!
*/
CREATE OR REPLACE TRIGGER csoportok_auto_increment_pk
  BEFORE INSERT ON csoportok
  FOR EACH ROW
  BEGIN
    :NEW.ID := csoportok_pk.NEXTVAL;
  END;

CREATE OR REPLACE TRIGGER felhasznalok_auto_increment_pk
  BEFORE INSERT ON felhasznalok
  FOR EACH ROW
  BEGIN
    :NEW.ID := felhasznalok_pk.NEXTVAL;
  END;

CREATE OR REPLACE TRIGGER msg_auto_increment_pk
	BEFORE INSERT ON uzenetek
	FOR EACH ROW
	BEGIN
		:NEW.ID := msg_pk.NEXTVAL;
	END;

CREATE OR REPLACE TRIGGER conv_auto_increment_pk
	BEFORE INSERT ON beszelgetesek
	FOR EACH ROW
	BEGIN
		:NEW.ID := beszelgetesek_pk.NEXTVAL;
	END;

CREATE OR REPLACE TRIGGER kepek_auto_increment_pk
  BEFORE INSERT ON kepek
  FOR EACH ROW
  BEGIN
      :NEW.id := kepek_pk.NEXTVAL;
  END;

CREATE OR REPLACE TRIGGER new_kep_for_user
  AFTER INSERT ON kepek
  FOR EACH ROW
  DECLARE
    felhasznalo_neve felhasznalok.NEV%TYPE;
  BEGIN
    SELECT nev INTO felhasznalo_neve FROM felhasznalok WHERE id = :NEW.felhasznalo;
    INSERT INTO posztok VALUES(posztok_pk.NEXTVAL, :NEW.felhasznalo, felhasznalo_neve || ' feltöltött egy új képet.', SYSDATE, :NEW.id);
  END;

CREATE OR REPLACE TRIGGER posztok_auto_increment_pk
  BEFORE INSERT ON posztok
  FOR EACH ROW
  BEGIN
    :NEW.id := posztok_pk.NEXTVAL;
  END;


  DROP TABLE "UZENETEK" CASCADE CONSTRAINTS;
	DROP TABLE "POSZTOK" CASCADE CONSTRAINTS;
	DROP TABLE "KEPEK" CASCADE CONSTRAINTS;
	DROP TABLE "ISMEROSOK" CASCADE CONSTRAINTS;
	DROP TABLE "FELHASZNALOK" CASCADE CONSTRAINTS;
	DROP TABLE "CSOPORT_TAGOK" CASCADE CONSTRAINTS;
	DROP TABLE "CSOPORTOK" CASCADE CONSTRAINTS;
	DROP TABLE "BESZELGETESEK" CASCADE CONSTRAINTS;
	DROP TABLE "HOZZASZOLASOK" CASCADE CONSTRAINTS;

	DROP VIEW V_NEWS_FEED;
	DROP VIEW V_NEWS_FEED_COMMENTS;

	DROP SEQUENCE BESZELGETESEK_PK;
	DROP SEQUENCE CSOPORTOK_PK;
	DROP SEQUENCE FELHASZNALOK_PK;
	DROP SEQUENCE MSG_PK;
	DROP SEQUENCE KEPEK_PK;
	DROP SEQUENCE POSZTOK_PK;